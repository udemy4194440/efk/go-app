package main

import (
    "log"
    "go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"time"
	"net/http"
)

func main() {
	loggerConfig := zap.NewProductionConfig()
    loggerConfig.EncoderConfig.TimeKey = "time"
    loggerConfig.EncoderConfig.EncodeTime = zapcore.TimeEncoderOfLayout(time.RFC3339)
	loggerConfig.EncoderConfig.CallerKey = "service"
	loggerConfig.EncoderConfig.MessageKey = "message"
	
    logger, err := loggerConfig.Build()
    if err != nil {
        log.Fatal(err)
    }

    logger.Info("Hello from zap logger")
	logger.Info("Also from zap logger")
	logger.Info("Again from zap logger")

	http.ListenAndServe(":5000", nil)
}